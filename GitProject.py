#! /usr/bin/python3
""" Copyright Ariana Giroux, March 2018 under the MIT License.

Copies predefined .gitignore or license files to the specified path.

The software will first look for a folder named '.ignoretemplates' (for
.gitignore files) or '.licensetemplates' (for license files) in the user's home
directory and gather any file types (if any), then will search the script
directory for defaults.  The software will always prefer files from the home
directory over the script directory.

The files contained in '.gittemplates' (for .gitignore files) or
'.licensetemplates' (for license files) directories must all be either git
ignore files that are to be treated as .gitignore files or license files. Each
file name represents the style of .gitignore or license that it is meant, where
a file named 'python3' would have python3 specific .gitingore settings; or where
a file named 'MIT' represents the MIT license.
"""

import argparse
import os
import sys
from shutil import copy2

from log import base_logger

################################################################################
#                             GLOBALS & ARGPARSE                               #
#                                                                              #
#            Define various globals, as well as argparse settings              #
#                                                                              #

SCRIPT_PATH = os.path.split(os.path.realpath(__file__))[0]

HOME_PATH = os.path.expanduser('~')

IGNORE_PATH = ".ignore"
LICENSE_PATH = ".licenses"
ARGPARSE_DESCRIPTION = """Copies predefined .gitignore or license files to the specified path.

The software will first look for a folder named '.ignoretemplates' (for
.gitignore files) or '.licensetemplates' (for license files) in the user's home
directory and gather any file types (if any), then will search the script
directory for defaults.  The software will always prefer files from the home
directory over the script directory.

The files contained in '.gittemplates' (for .gitignore files) or
'.licensetemplates' (for license files) directories must all be either git
ignore files that are to be treated as .gitignore files or license files. Each
file name represents the style of .gitignore or license that it is meant, where
a file named 'python3' would have python3 specific .gitingore settings; or where
a file named 'MIT' represents the MIT license.
"""

parser = argparse.ArgumentParser(description=ARGPARSE_DESCRIPTION)
subparser = parser.add_subparsers()
subparser.dest = 'sub_command'

license_parser = subparser.add_parser('license',
                                      help='Copy a license file')

license_parser.add_argument("license_type", help='The type of license to '
                            'generate.', type=str)

license_parser.add_argument("-p", "--path", help="The path to output the "
                            "license to. Defaults to the current directory",
                            type=str)

license_parser.add_argument('--types', help='Simply list the available '
                            'license files.', action='store_true')

ignore_parser = subparser.add_parser('ignore',
                                     help='Copy an ignore file.')

ignore_parser.add_argument("project_type", help="The type of project to "
                           "generate a .gitignore file for.", type=str)

ignore_parser.add_argument("-p", "--path", help="The path to output the "
                           "gitignore to. Defaults to the current "
                           "directory.", type=str)

ignore_parser.add_argument("--types", help="Simply list the "
                           "available ignore types.", action="store_true")


################################################################################
#                              LOGIC FUNCTIONS                                 #

def list_types(path):
    """ Returns a list of file names located at `path`.

    `path` - The path to retrieve files from.
    `returns` - A dict of the following structure:
        {
            'home': `the contents of os.listdir(path)`
            'script': `the contents of os.listdir(path)`
        }
    """
    types = {
        'home': None,
        'script': None,
    }

    try:
        if os.access(os.path.join(HOME_PATH, path), os.F_OK):
            types['home'] = os.listdir(os.path.join(HOME_PATH, path))
            base_logger.debug('home: %s', types['home'])

        if os.access(os.path.join(SCRIPT_PATH, path), os.F_OK):
            types['script'] = os.listdir(os.path.join(SCRIPT_PATH, path))
            base_logger.debug('script: %s', types['script'])
    except Exception as e:
        base_logger.critical(str(e))
        raise

    return types


def ignore_copy(file, path=None):
    """ Copies a .gitignore file from either the home or script directory that
    shares a name with `file` to the optional `path`. If `path` is not
    specified, copy to the current directory.

    `file` - The file name to attempt to copy.
    `path` - The path to copy the file to.
    """

    if path is None:  # Handle None
        path = ''

    types_list = list_types(IGNORE_PATH)
    if types_list['home'] is not None and types_list['home'].count(file):
        try:
            copy2(os.path.join(HOME_PATH, IGNORE_PATH, file),
                  os.path.join(path, '.gitignore'))
        except Exception as e:
            base_logger.critical(str(e))
            raise

        base_logger.info('Copied %s to %s'
                         % (os.path.join(HOME_PATH, IGNORE_PATH, file),
                            os.path.join(path, '.gitignore')))

    elif types_list['script'] is not None and types_list['script'].count(file):
        try:
            copy2(os.path.join(SCRIPT_PATH, IGNORE_PATH, file),
                  os.path.join(path, '.gitignore'))
        except Exception as e:
            base_logger.critical(str(e))
            raise

        base_logger.info('Copied %s to %s'
                         % (os.path.join(SCRIPT_PATH, IGNORE_PATH, file),
                            os.path.join(path, '.gitignore')))
    else:
        base_logger.warning('Could not find \'%s\'' % file)


def license_copy(file, path=None):
    """ Copies a LICENSE file from either the home or script directory that
    shares a name with `file` to the optional `path`. If `path` is not
    specified, copy to the current directory.

    `file` - The file name to attempt to copy.
    `path` - The path to copy the file to.
    """

    if path is None:  # Handle None
        path = ''

    types_list = list_types(LICENSE_PATH)
    if types_list['home'] is not None and types_list['home'].count(file):
        try:
            copy2(os.path.join(HOME_PATH, LICENSE_PATH, file),
                  os.path.join(path, 'LICENSE'))
        except Exception as e:
            base_logger.critical(str(e))
            raise

        base_logger.info('Copied %s to %s'
                         % (os.path.join(HOME_PATH, LICENSE_PATH, file),
                            os.path.join(path, 'LICENSE')))

    elif types_list['script'] is not None and types_list['script'].count(file):
        try:
            copy2(os.path.join(SCRIPT_PATH, LICENSE_PATH, file),
                  os.path.join(path, 'LICENSE'))
        except Exception as e:
            base_logger.critical(str(e))
            raise

        base_logger.info('Copied %s to %s'
                         % (os.path.join(SCRIPT_PATH, LICENSE_PATH, file),
                            os.path.join(path, 'LICENSE')))
    else:
        base_logger.warning('Could not find \'%s\'' % file)


if __name__ == "__main__":
    ############################################################################
    #                              OUTPUT TYPES                                #
    if len(sys.argv) > 0 and sys.argv.count('--types'):
        if sys.argv.count('ignore'):  # User supplied `license --types`
            base_logger.disabled = True  # DEBUG.

            types_list = list_types(IGNORE_PATH)

            print('Files found in home directory (%s):\n    %s'
                  % (os.path.join(HOME_PATH, IGNORE_PATH), types_list['home']))

            print('Files found in script directory (%s):\n    %s'
                  % (os.path.join(SCRIPT_PATH, IGNORE_PATH),
                     types_list['script']))

            base_logger.disabled = False
            exit()

        elif sys.argv.count('license'):
            base_logger.disabled = True
            types_list = list_types(LICENSE_PATH)
            print('Files found in home directory (%s):\n    %s'
                  % (os.path.join(HOME_PATH, LICENSE_PATH), types_list['home']))
            print('Files found in script directory (%s):\n    %s'
                  % (os.path.join(SCRIPT_PATH, LICENSE_PATH),
                     types_list['script']))
            base_logger.disabled = False
            exit()

    parsed = parser.parse_args()

    if parsed.sub_command == 'license':
        license_copy(parsed.license_type, parsed.path)

    elif parsed.sub_command == 'ignore':
        ignore_copy(parsed.project_type, parsed.path)

    else:
        parser.print_usage()
