# Description

Provides easy local templating for `.gitignore` files and license files.

Provides a few default template files for example or use, but it is highly recommended to implement your own template files.

The script looks for template files in `~/.ignoretemplates` and `~/licensetemplates` for ignore files and license files respectively.

All files in these directories should be named by there template use. For example, a `.gitignore` template for Java might be placed under the path `~/ignoretemplates/Java`, but will be copied under the name `.gitignore`. The same is true for license templates being copied as `TEMPLATE`.

# Usage

From the script:
```
usage: GitProject.py [-h] {license,ignore} ...

Copies predefined .gitignore or license files to the specified path. The
software will first look for a folder named '.ignoretemplates' (for .gitignore
files) or '.licensetemplates' (for license files) in the user's home directory
and gather any file types (if any), then will search the script directory for
defaults. The software will always prefer files from the home directory over
the script directory. The files contained in '.gittemplates' (for .gitignore
files) or '.licensetemplates' (for license files) directories must all be
either git ignore files that are to be treated as .gitignore files or license
files. Each file name represents the style of .gitignore or license that it is
meant, where a file named 'python3' would have python3 specific .gitingore
settings; or where a file named 'MIT' represents the MIT license.

positional arguments:
  {license,ignore}
    license         Copy a license file
    ignore          Copy an ignore file.

optional arguments:
  -h, --help        show this help message and exit
```

For license copying:
```
usage: GitProject.py license [-h] [-p PATH] [--types] license_type

positional arguments:
  license_type          The type of license to generate.

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  The path to output the license to. Defaults to the
                        current directory
  --types               Simply list the available license files.
```

For .gitignore copying:
```
usage: GitProject.py ignore [-h] [-p PATH] [--types] project_type

positional arguments:
  project_type          The type of project to generate a .gitignore file for.

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  The path to output the gitignore to. Defaults to the
                        current directory.
  --types               Simply list the available ignore types.
```

# Installation

The script only requires `Python3`'s standard library, making installation easy.

The script will not create the `~/.ignoretemplate` or `~/.licensetemplate` paths itself, so you'll have to do that if you want to use the home directory features.

Optionally, you can set the interpreter line to your own path and execute this file directly. Interpreter defaults to `/usr/local/bin/python3`.
